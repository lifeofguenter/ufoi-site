---
title: Council Elections
---

Council members will largely play an administrative role. Their aim will be to conduct elections, see about the due process, and get users and instances registered in the system. All important decisions will ultimately be carried out by a vote open to all members of the UFoI.

The number of council members will always be 1/10th of the number of instances in the UFoI, rounded down. Open seats will be voted on through an open nomination and election process which will be completely transparent and recorded on GitLab for preservation.

For complete information on the procedures for council elections please see the [full bylaws](https://ufoi.gitlab.io/constitution/united_federation_of_instances_bylaws.pdf).
