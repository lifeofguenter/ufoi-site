---
title: Amendments
---

The [bylaws](/docs/bylaws) of the UFoI, which codifies the UFoI rules as well as its [Code of Ethics](/docs/code-of-ethics) is a living document. At any time a change to the bylaws can be suggested and passed by a 2/3rds majority vote.

The first step is to go to the [GitLab repo](https://gitlab.com/ufoi/constitution) where the official bylaws are defined and creating a Merge Request. At this point you must require enough support to bring the amendment to vote, this is done by having members of the UFoI sign the Merge Request. Once ample support is raised a full vote is called.

For more information see the [official bylaws](https://ufoi.gitlab.io/constitution/united_federation_of_instances_bylaws.pdf) which outlines the full procedure and requirements.
