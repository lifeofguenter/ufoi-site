import {
  Button,
  Card,
  Heading4,
  Heading2,
  InlineLink,
  Input,
  LabelText,
  Text,
} from '@algolia/ui-library';
import { useBaseUrlUtils } from '@docusaurus/useBaseUrl';
import React, { useState } from 'react';
import instanceMembers from './instance-members.json';
import userMembers from './user-members.json';

function UserMember(props) {
  const { withBaseUrl } = useBaseUrlUtils();

  return (
    <div className="profile-content">
      {userMembers.map(({ name, display_name, profiles, pgp_fingerprint, instance_member_reps, on_council, on_ufoi_instance, emails }) => name === props.name && (
        <Card className="uil-m-auto apply-form text-left">
          <Heading2 className="apply-text">{display_name}</Heading2>
          { on_ufoi_instance ? (
            <div>
            <i>This user has an account on an instance that is a member of the UFoI, therefore, they, and their entire instance, agree to uphold the UFoI <a href={withBaseUrl('/docs/code-of-ethics')} alt="Code of Ethics">Code of Ethics</a> at all times.</i>
            <br />
            <br />
            <div>UFoI Instance Member: true</div>
            </div>
          ) : (
            <div>
            <i>This user is <b>not</b> on an instance that is a member of the UFoI. However, they have made a personal pledge to uphold the UFoI <a href={withBaseUrl('/docs/code-of-ethics')} alt="Code of Ethics">Code of Ethics</a> at all times.</i>
            <br />
            <br />
            <div>UFoI Instance Member: false</div>
            </div>
          )}
          { on_council ? (
            <div>Council Member: true</div>
          ) : (
            <div>Council Member: false</div>
          )}
          {profiles.map(profile => (
            <div>
              Profile:
              <a
                href={profile.url}
                rel="me"
                target="_blank"
                alt={`User's profile link`}
                >
                  {profile.handle}
              </a>
              <br />
            </div>
          ))}
          { emails && (
            <div>
              {emails.map(email => (
                <div>
                  Email: {email}
                </div>            
              ))}
            </div>
          )}
          { instance_member_reps && (
            <div>
              {instance_member_reps.map(instance_member_rep => (
                <div>
                  Instance Member Representation: {instance_member_rep}
                </div>            
              ))}
            </div>
          )}
        </Card>
      ))}
    </div>
  );
}

export default UserMember;
