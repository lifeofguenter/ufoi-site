import { Hero } from '@algolia/ui-library';
import Layout from '@theme/Layout';
import React from 'react';

import InstanceMemberRep from '../components/InstanceMemberRep.js';
import SiteLogo from '../components/SiteLogo';

function InstanceMemberRepPage() {
  return (
    <Layout
      title="UFoI: Instance Member Representatives"
      description="A federation of good-faith actors on the Fediverse"
    >
      <div className="uil-pb-24">
        <Hero
          id="instance-member-reps"
          title={<SiteLogo width="100%" />}
          background="curves"
        />
        <InstanceMemberRep />
      </div>
    </Layout>
  );
}

export default InstanceMemberRepPage;
